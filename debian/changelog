libgtk3-imageview-perl (12-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 12.
  * Update upstream copyright holders and years.
  * Update debian/upstream/metadata.
  * Drop given_when.patch, which was taken from a now applied
    pull request.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Nov 2024 03:19:05 +0100

libgtk3-imageview-perl (10-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ gregor herrmann ]
  * Add patch to replace deprecated given/when with if/elsif/else.
    (Closes: #1050445)
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Nov 2023 20:56:48 +0100

libgtk3-imageview-perl (10-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 10.
  * Update upstream copyright notice.
  * Drop debian/tests/pkg-perl/smoke-skip. The skipped test is gone.
  * Update test and runtime dependencies.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Wed, 13 Oct 2021 19:52:25 +0200

libgtk3-imageview-perl (9-2) unstable; urgency=medium

  * Add the test dependency libtest-deep-perl.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 30 Aug 2021 12:45:06 +0100

libgtk3-imageview-perl (9-1) unstable; urgency=medium

  * New upstream release.
  * Update the upstream metadata.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 30 Aug 2021 08:29:08 +0100

libgtk3-imageview-perl (6-1) unstable; urgency=medium

  * Import upstream version 6.

 -- Jeffrey Ratcliffe <jjr@debian.org>  Fri, 20 Nov 2020 12:35:15 +0100

libgtk3-imageview-perl (4-1) unstable; urgency=medium

  * Team upload.
  * Source-only upload.
  * Import upstream version 4.
  * Update build and test dependencies.
  * Update short and long description following upstream.
  * Fix autopkgtests.
    Add debian/tests/pkg-perl/{smoke,syntax}-skip to skip a smoke test and the
    syntax test for some modules.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Wed, 28 Oct 2020 18:10:36 +0100

libgtk3-imageview-perl (1-1) unstable; urgency=low

  * Initial release. Closes: #972643

 -- Jeffrey Ratcliffe <jjr@debian.org>  Wed, 21 Oct 2020 19:51:00 +0200
